# businesscards.exe

Storing useful and novelty software on simple paper or plastic? More likely than you think.

I like the idea of ultra-compact software. Small tools or (most prevalently) tech demos that fit in a couple hundred or thousand bytes.  
That's so small you can print the entire program on a small piece of paper, maybe the size of a standard (85x55 mm) business card. At this size, you can see the (literal) bits with the naked eye... and so can a smartphone camera and a QR Code scanning app.

Of all the nice small programs I find online, I will make printable images for myself and collect them here. I might also create my own original tiny programs in the future, and those will go here too.

For the collected programs, stored in their directory, credits to the authors (in form of a link to the website i found the code on) are written in the images. Every one of them might have a specific license, so check the links if you so desire.

I create all the printable images myself manually, so they (and their source if available), are licensed under Creative Commons Attribution-ShareAlike 4.0 International, by my choice.

### About printing

All the images for the individual cards are sized so that, when imported in GIMP in a new document created from any preset specifying sheet size at 300 DPI, instead of resolution (ex. "A4 paper at 300 DPI" instead of "Full HD 1280x720"), they are correctly scaled as I intended.

Some images (those with white background) are indeed designed to be exactly 85x55 mm, others (ones with colored background) may be just a bit larger, so that they get printed with a bleed zone and can be cut to size without worry of showing white near the cut zone.
